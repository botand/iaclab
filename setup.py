#!/usr/bin/env python

"""The setup script."""
from setuptools import setup, find_packages

with open("README.md") as readme_file:
    readme = readme_file.read()

test_requirements = ["pytest>=3"]

setup(
    description="IACLAB CLI utility.",
    author="Andreas Botzner",
    license="GPLv3",
    author_email="a.botzner@endian.com",
    url="https://gitlab.com/botand/iaclab",
    keywords=[
        "iaclab",
        "proxmox",
        "package",
    ],
    python_requires=">=3.6",
    long_description=readme,
    include_package_data=True,
    test_suite="tests",
    tests_require=test_requirements,
    zip_safe=False,
)
