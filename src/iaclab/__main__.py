"""IACLAB entry point."""
from iaclab.shell import main

if __name__ == "__main__":
    main()
