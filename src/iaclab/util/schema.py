"""YAML validation schemas.

File: schema.py
Author: Andreas Botzner
Email: a.botzner@endian.com
Description: Schemas for validating config files.
"""
import logging
import sys

from schema import Regex, Schema, SchemaError, And, Or, Optional

LOG = logging.getLogger(__name__)
naming_regex = r"^[a-zA-Z][a-zA-Z0-9-.]*[a-zA-Z0-9]$"
nic_regex = r"^net[0-4]{1}$"

ENV_SCHEMA = Schema(
    {
        "iaclab_env_name": And(str, Regex(naming_regex)),
        "interfaces": [{"name": Regex(naming_regex), "cidr": str}],
        "vms": [
            {
                "name": Regex(naming_regex),
                "template": str,
                Optional("memory"): And(
                    int,
                    lambda n: 256 <= n <= 16384,
                    error="Memory must be integer and between 256 and 16384.",
                ),
                Optional("cores"): And(
                    int,
                    lambda n: 1 <= n <= 8,
                    error="cores must be integer and between 1 and 8.",
                ),
                "interfaces": [
                    {
                        "nic": And(str, Regex(nic_regex)),
                        "name": str,
                        Optional("firewall"): str,
                        Optional("model"): Or(
                            "virtio" "e1000",
                            "rtl8139",
                            error='Unsupported interface model. Supported models are:"virtio", "e1000", "rtl8139".',
                        ),
                    }
                ],
            }
        ],
    }
)

CONF_SCHEMA = Schema(
    {
        "proxmox_user": str,
        "proxmox_password": str,
        "proxmox_host": str,
        "proxmox_node": str,
        "redis_user": str,
        "redis_password": str,
        "redis_host": str,
        "hetzner_dns_api_token": str,
        "mgmt_cidr": str,
        "domain": str,
    }
)


def validate(obj, schema: Schema):
    """Validate object against schema."""
    try:
        schema.validate(obj)
    except SchemaError as e:
        LOG.error("Error validating schema!")
        for error in e.errors:
            if error:
                LOG.warning(str(e))
        for error in e.autos:
            if error:
                LOG.warning(str(e))
        sys.exit(-1)
