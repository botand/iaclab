"""IACLAB click shell module."""
import click
import packaging
import sys
import os
import xdg
import logging

import iaclab

from iaclab import command, logger
from iaclab.command.base import click_custom_group
from iaclab.console import console


def print_version(ctx, param, value):
    """Print version."""
    if not value:
        return
    v = packaging.version.Version(iaclab.__version__)
    msg = f"iaclab [green]{v}[/] using python [repr.number]{sys.version_info[0]}.{sys.version_info[1]}[/] \n"
    console.print(msg)
    ctx.exit()


@click_custom_group()
@click.pass_context
@click.option(
    "--debug/--no-debug",
    default=False,
    help="Enable or disable debug mode.",
)
@click.option(
    "--config",
    "-c",
    multiple=False,
    type=click.Path(exists=True, file_okay=True,
                    readable=True, allow_dash=False),
    help=("Path to the base IACLAB configuration."),
)
@click.option(
    "--version", is_flag=True, callback=print_version, expose_value=False, is_eager=True
)
def main(ctx, debug, config):
    """Iaclab allows to create complex virtual environments on Proxmox."""
    ctx.obj = {}
    ctx.obj["args"] = {}
    ctx.obj["args"]["debug"] = debug
    if config is None:
        config = os.path.join(xdg.XDG_CONFIG_HOME, "iaclab.yml")
    ctx.obj["args"]["config"] = config
    if debug:
        logging.getLogger().setLevel(logging.DEBUG)


main.add_command(command.init.init)
