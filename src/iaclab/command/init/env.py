"""Init Command Module."""

import os
import sys
import logging

import click

from iaclab.util import templates
from iaclab.command import base


LOG = logging.getLogger(__name__)


class Env(base.Base):
    """
    Environment init command class.

    Initialize environment configuration file to bootstrap
    custom iaclab environments.

    """

    def __init__(self, command_args) -> None:
        """Construct Environment."""
        self._command_args = command_args

    def execute(self):
        """Execute commands to generate environment config file.

        :return: None
        """
        env_name = self._command_args["env_name"]
        output_file = self._command_args["file"]
        force = self._command_args["force"]
        if output_file is None:
            output_file = os.path.abspath(
                os.path.join(os.getcwd(), env_name + ".yml"))
        else:
            output_file = os.path.abspath(output_file)

        LOG.debug("Set output file to :{0}".format(output_file))
        LOG.info("Initializing environment config for {0}...".format(env_name))
        if os.path.exists(output_file) and force is not True:
            msg = (
                "Config file {0} already exists."
                "Cannot initialize environment config file."
            ).format(output_file)
            LOG.critical(msg)
            sys.exit(-1)
        args = {"env_name": env_name, "env_file": output_file}
        processor = templates.FileProcessor("env.yml", args, output_file)
        processor.process()
        msg = "Successfully initialized environment config for {0} in {1}".format(
            env_name, output_file
        )
        LOG.info(msg)


@click.command()
@click.pass_context
@click.option(
    "--force",
    "-f",
    is_flag=True,
    default=False,
    help="Overwrite existing config.",
)
@click.option(
    "--file",
    type=click.Path(file_okay=True, writable=True, allow_dash=False),
    help="File to write the generated config into.",
    default="iaclab.yml",
)
@click.argument("ENV-NAME", type=str, required=True)
def env(ctx, force, file, env_name):
    """Generate environment config skeleton.

    Creates a IACLAB environment config file with some default values.
    """
    args = ctx.obj.get("args")
    subcommand = base._get_subcommand(__name__)
    args.update(
        {"env_name": env_name, "force": force, "file": file, "subcommand": subcommand}
    )
    r = Env(args)
    r.execute()
