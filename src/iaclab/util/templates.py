"""Template Processing Module."""

import logging
import os
import sys
from string import Template

import cookiecutter
import cookiecutter.main


LOG = logging.getLogger(__name__)


class CookieProcessor(object):
    """Cookiecutter template processor."""

    def __init__(self, template_dir: str, extra_context, output_dir, overwrite=True):
        """Constructor."""
        self.template_dir = self._resolve_template_dir(template_dir)
        self.extra_context = extra_context
        self.output_dir = self._validate_dir(output_dir)
        self.overwrite = overwrite

    def process(self):
        """
        Process cookiecutter templates.

        :param template_dir: A string witch directory to process.
        :param extra_context: Some extra context variables.
        :param output_dir: Dir where to output should be written to.
        :param overwrite: Whether to overwrite existing files and directories.
        :returns: None
        """
        LOG.debug(
            "Initializing templates at {0}".format(self.output_dir)
            + " with context {0}".format(self.extra_context)
        )
        try:
            cookiecutter.main.cookiecutter(
                self.template_dir,
                extra_context=self.extra_context,
                output_dir=self.output_dir,
                overwrite_if_exists=self.overwrite,
                no_input=True,
            )
        except Exception as e:
            LOG.warning("Applying templates resulted in an error!")
            LOG.warning(str(e))
            sys.exit(0)

    def _resolve_template_dir(self, template_dir: str) -> str:
        """Resolve the correct absolute path to the templates."""
        if not os.path.isabs(template_dir):
            template_dir = os.path.abspath(
                os.path.join(
                    os.path.dirname(__file__),
                    os.path.pardir,
                    "templates",
                    "cookiecutter",
                    template_dir,
                )
            )
        return template_dir

    def _validate_dir(self, path_to_dir: str) -> str:
        if not os.path.isdir(path_to_dir):
            LOG.error("Output dir {0} does not exist".format(path_to_dir))
            sys.exit(-1)
        return path_to_dir


class FileProcessor(object):
    """File template processor."""

    def __init__(self, template: str, args, output_file, overwrite=True):
        """Constructor."""
        self.template = self._resolve_template(template)
        self.args = args
        self.output_file = output_file
        self.overwrite = overwrite

    def process(self):
        """Apply file templates.

        :param template: A string with the template name.
        :param args: Data that the template is replaced with.
        :param output_file: Directory where to output should be written to.
        :param overwrite: Whether to overwrite existing files and directories.
        :returns: None
        """
        template = None
        LOG.debug(
            "Initializing templates at {0}".format(self.output_file)
            + " with context {0}".format(self.args)
        )
        try:
            with open(self.template, "r") as t, open(self.output_file, "w") as o:
                template = Template(t.read())
                s = template.safe_substitute(self.args)
                o.write(s)
        except Exception as e:
            LOG.error(
                "An exception occurred parsing the templates: {0}".format(str(e)))
            sys.exit(-1)

    def _resolve_template(self, template: str) -> str:
        """Resolve the correct absolute path to the template."""
        template = os.path.abspath(
            os.path.join(
                os.path.dirname(__file__),
                os.path.pardir,
                "templates",
                "files",
                template,
            )
        )
        return template
