"""Base class for the init command."""

import logging
import click

from iaclab.command import base
from iaclab.command.init import env, config, inventory

LOG = logging.getLogger(__name__)


@click.group()
def init():
    """Initialize environment or config files."""


init.add_command(env.env)
init.add_command(config.config)
init.add_command(inventory.inventory)
