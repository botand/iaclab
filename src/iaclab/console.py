"""Simple Console Module."""

from enrich.console import Console
from rich.style import Style
from rich.theme import Theme

theme = Theme(
    {
        "info": "dim bright_cyan",
        "warning": "yellow",
        "danger": "bold red",
        "action": "green",
        "section_title": "bold bright_cyan",
        "logging.level.notset": Style(dim=True),
        "logging.level.debug": Style(color="white", dim=True),
        "logging.level.info": Style(color="blue"),
        "logging.level.warning": Style(color="yellow"),
        "logging.level.error": Style(color="red", bold=True),
        "logging.level.critical": Style(color="red", bold=True),
        "logging.level.success": Style(color="green", bold=True),
    }
)

console = Console(
    redirect=True,
    record=True,
    soft_wrap=True,
)
