"""Base Command Module."""

import abc
import sys
import logging

import click

from click_help_colors import HelpColorsGroup
from iaclab import logger

LOG = logging.getLogger(__name__)
IACLAB_DEFAULT_ENV_NAME = "default"


class Base(object, metaclass=abc.ABCMeta):
    """An abstract base class used to define the command interface."""

    def __init__(self):
        """Command constructor."""
        pass

    @abc.abstractmethod
    def execute(self):
        """Execute command."""
        pass


def click_custom_group():
    """Return extended version of click.group()."""
    return click.group(
        cls=HelpColorsGroup,
        context_settings=dict(max_content_width=9999, color=True),
        result_callback=result_callback,
    )


def _get_subcommand(string):
    return string.split(".")[-1]


def result_callback(*args, **kwargs):
    """Exit normal callback."""
    sys.exit(0)
