# Iaclab

> A CLI tool to generate IACLAB Ansible Inventories from YAML files.

## Installation

Clone the repository and run:

```bash
./setup.py install
```

## Usage

`iaclab` supports three commands:

-   `init config`
-   `init env`
-   `init inventory`

### init config

This command creates an empty global configuration file in XDG_CONFIG_HOME.
All before running `init inventory` a global configuration file has to be
created and the default values have to be replaces with real ones.

### init env

`init env <env_name>` creates a new IACLAB environment configuration file.
This file contains a simple configuration with one network and
one VM inside. This file can be used as a starting point when creating new
IACLAB environment configuration files. By default the file will be named
`iaclab.yml`.

### init inventory

This is the core command of the `iaclab` CLI tool. It is used to generate
Ansible Inventory folder structures from IACLAB YAML configuration files.

To generate an Ansible Inventory out of an IACLAB environment config file,
simply run:

```bash
iaclab init inventory iaclab.yml
```

This will then generate a folder named after the environment (custom paths can
also be specified). The generated folder will have a basic Ansible Inventory
folder structure like so:

```bash
.
├── ansible.cfg
├── destroy.yml
├── requirements.yml
├── start.yml
├── group_vars
│   └── all
│       └── main.yml
├── host_vars
│   ├── vm-1-name.pve-user.env-name.iaclab.io
│   │   └── main.yml
│   ├── file
│   └── vm-2-name.pve-user.env-name.iaclab.io
│       └── main.yml
└── inventory
    └── hosts
```

The `requirements.yml` file contains a list of Ansible Roles an collections,
that are required for the role. To install them simply run

```
ansible-galaxy install -r requirements.yml
```

After the dependencies are installed one can execute the `start.yml` playbook
to bootstrap the IACLAB environment.

Once done with the environment, simply run the `destroy.yml` playbook do clean
up any VMs.
