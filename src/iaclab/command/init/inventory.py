"""Inventory Generator Module."""


import os
import sys
import logging
import click
import schema as ss

from iaclab.util import util, schema
from iaclab.util.templates import CookieProcessor

from iaclab.command import base

LOG = logging.getLogger(__name__)


class Inventory(base.Base):
    """Generate Ansible inventory from a config file."""

    def __init__(self, args) -> None:
        """Initialize command class and perform sanity checks.

        @param args:  List of arguments passed to the command.
        @type  args:  dict
        """
        self._command_args = args
        self._force = args["force"]
        self._config = self._validate_config(
            args["file"], "env config", schema.ENV_SCHEMA
        )
        self._output_path = self._validate_output_path(args["path"])
        self._config.update(
            self._validate_config(args["config"], "config", schema.CONF_SCHEMA)
        )

    def _validate_output_path(self, path: str) -> str:
        """Check if folder with the environment name exists in output path."""
        if (os.path.exists(os.path.join(path, self._config["iaclab_env_name"]))
                and not self._force):
            msg = (
                "Directory {0} already exists."
                "Cannot initialize inventory structure."
            ).format(path)
            LOG.critical(msg)
            sys.exit(-1)
        return path

    def _validate_config(self, file: str, name: str, s: ss.Schema) -> dict:
        """Read in YAML file and test against schema.

        @param file:  Path to the YAML file
        @type  file:  str
        @param name:  Description of `file` content.
        @type  name:  str
        @param s:  Schema to test the content of `file` against`.
        @type  s:  Schema

        @return:  Parsed content of the YAML file. Fixed to dict.
        @rtype : dict

        """
        config = util.read_yaml(file)
        LOG.info("Validating {0} file.".format(name))
        schema.validate(config, s)
        LOG.info("Successfully validated {0} file.".format(name))
        return config

    def _gen_inventory(self):
        """Generate base inventory folder structure."""
        args = {
            "vm_list": {"vms": [vm["name"] for vm in self._config["vms"]]},
            "user": self._config["proxmox_user"].split("@")[0],
        }
        LOG.info(args)
        args.update(self._config)
        LOG.debug("Generating inventory...")

        inventory = CookieProcessor(
            "inventory", args, self._output_path, self._force)
        inventory.process()
        LOG.info("Successfully generated inventory.")

    def _gen_host_vars(self):
        """Generate host_vars folders and files."""
        host_vars_path = os.path.join(
            self._output_path, self._config["iaclab_env_name"], "host_vars"
        )
        LOG.info("Generating host_vars...")
        for vm in self._config["vms"]:
            LOG.debug("Generating host_vars for {0}".format(vm["name"]))
            hostname = "{0}.{1}.{2}.{3}".format(
                vm["name"],
                self._config["proxmox_user"].split("@")[0],
                self._config["iaclab_env_name"],
                self._config["domain"],
            )
            cutter = CookieProcessor(
                "host_vars", {
                    "hostname": hostname}, host_vars_path, self._force
            )
            cutter.process()
        LOG.info("Successfully generated host_vars.")

    def _gen_group_all(self):
        LOG.info("Generating group_vars/all...")
        util.write_yaml(
            self._config,
            os.path.join(
                self._output_path,
                self._config["iaclab_env_name"],
                "group_vars",
                "all",
                "main.yml",
            ),
        )
        LOG.info("Successfully generated group_vars/all.")

    def execute(self):
        """Execute commands to generate inventory."""
        LOG.info(
            "Initializing environment config for {0}...".format(
                self._config["iaclab_env_name"]
            )
        )
        self._gen_inventory()
        self._gen_host_vars()
        self._gen_group_all()

        LOG.info("Generation successful: {0}".format(self._output_path))


@click.command()
@click.pass_context
@click.option(
    "--force",
    "-f",
    is_flag=True,
    default=False,
    help="Overwrite existing files.",
)
@click.option(
    "--path",
    "-p",
    type=click.Path(dir_okay=True, writable=True, allow_dash=False),
    help="Path where to generate inventory.",
    default=os.getcwd(),
)
@click.argument(
    "FILE",
    required=True,
    type=click.Path(exists=True, file_okay=True,
                    allow_dash=False, readable=True),
)
def inventory(ctx, force, path, file):
    """Generate Ansible inventory folder structure.

    Create a inventory folder structure out of the specified
    environment config file. Complete with host_vars, group_vars
    and the playbooks to start/destroy the environment.
    """
    args = ctx.obj.get("args")
    subcommand = base._get_subcommand(__name__)
    LOG.debug("Generating Inventory")
    args.update({"file": file, "force": force,
                "path": path, "subcommand": subcommand})
    r = Inventory(args)
    r.execute()
