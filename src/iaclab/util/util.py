"""Utility functions for IACLAB.

File: util.py
Author: Andreas Botzner
Email: a.botzner@endian.com
Description: Utility module.
"""

import logging
import os
import sys
import yaml

LOG = logging.getLogger(__name__)


def read_yaml(file: str) -> dict:
    """Read in YAML file.

    @param file:  Path to YAML file.
    @type  file:  str

    @return:  Data read from the file. Empty dict if file empty.
    @rtype :  dict

    """
    data = {}
    try:
        with open(file, "r") as yaml_file:
            content = yaml_file.read()
            data = yaml.safe_load(content)
    except Exception as e:
        LOG.error("Error reading YAML file: {0}".format(file))
        LOG.error(str(e))
        sys.exit(-1)
    return data


def write_yaml(data: dict, file: str):
    """Write dictionary data to YAML file.

    @param data:  Dictionary with data.
    @type  data:  dict
    @param file:  Absolute path to file.
    @type  file:  str
    """
    try:
        LOG.debug("Writing data to: {0}".format(file))
        with open(file, "w") as f:
            yaml.safe_dump(data, f)  # type: ignore
    except Exception as e:
        LOG.error("Failed to write data to: {0}".format(file))
        LOG.warn("The error was: {0}".format(str(e)))
        sys.exit(-1)
    LOG.debug("Successfully wrote data.")
