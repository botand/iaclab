"""Config Init Command Module."""

import os
import sys
import logging
import xdg

import click

from iaclab.util import templates
from iaclab.command import base

LOG = logging.getLogger(__name__)


class Env(object):
    """Config init command class.

    Initialize iaclab configuration file.

    """

    def __init__(self, command_args) -> None:
        """Construct config."""
        self._command_args = command_args

    def execute(self):
        """Execute commands to generate iaclab config file.

        :return: None
        """
        output_file = self._command_args["file"]
        force = self._command_args["force"]
        if output_file is None:
            if xdg.XDG_CONFIG_HOME:
                output_file = os.path.join(xdg.XDG_CONFIG_HOME, "iaclab.yml")
            else:
                output_file = os.path.join("~/", "iaclab.yml")
        else:
            output_file = os.path.abspath(output_file)

        LOG.debug("Set output file to :{0}".format(output_file))
        LOG.info("Initializing iaclab config")
        if os.path.exists(output_file) and force is not True:
            msg = (
                "Config file {0} already exists."
                "Cannot initialize environment config file."
            ).format(output_file)
            LOG.critical(msg)
            sys.exit(-1)
        processor = templates.FileProcessor("config.yml", None, output_file)
        processor.process()
        msg = "Successfully initialized iaclab config at {0}".format(output_file)
        LOG.info(msg)


@click.command()
@click.pass_context
@click.option(
    "--force",
    "-f",
    is_flag=True,
    default=False,
    help="Overwrite existing config.",
)
@click.option(
    "--file",
    type=click.Path(file_okay=True, writable=True, allow_dash=False),
    help="File to write the generated config into.",
)
def config(ctx, force, file):
    """Initialize default config file.

    Creates empty configuration file for iaclab. By default
    the file will be generated in XDG_CONFIG_HOME/iaclab.yml.
    All variables in the file have to be defined.
    """
    args = ctx.obj.get("args")
    subcommand = base._get_subcommand(__name__)
    args.update({"force": force, "file": file, "subcommand": subcommand})
    r = Env(args)
    r.execute()
